#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <bitset>
#include <algorithm>
#include "getvalue.h"

extern std::string g_input_file_name;
extern std::string g_output_file_name;
extern std::string g_transmitted_file_name;
extern const char polynomial;
extern const int polynomial_length;

enum AddCheck {ADD, CHECK, TRANSMIT};
enum Function {PARITYBIT, CHECKSUM, CRC, NOISE};

// Main function
bool AddCheckFunction(AddCheck add_or_check, Function function, bool repetition = NULL, short percentage = NULL);

unsigned long long ParityBit(unsigned char* buffer, int packet_length);
unsigned long long Checksum(unsigned char* buffer, int &packet_length);
void ChecksumCRC(unsigned char* buffer, int &packet_length, char &check_byte);

// Noises
std::vector<unsigned long long> DrawNumbers(bool repetition, short percentage, unsigned long long &file_length);
void Noise(unsigned char* buffer, int &packet_length, std::vector<unsigned long long> &drawn_numbers, unsigned int &iterator, int &current_number_index);