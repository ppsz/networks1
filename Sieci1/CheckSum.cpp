#include "header.h"

unsigned long long Checksum(unsigned char* buffer, int &packet_length)
{
	unsigned long long check_bytes = 0, packet_check_bytes = 0;
	for (int j = 0; j < packet_length; ++j)
	{
		packet_check_bytes += buffer[j];
	}
	check_bytes += packet_check_bytes % 128;

	return check_bytes;
}