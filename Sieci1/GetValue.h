#pragma once

#include <iostream>
#include <limits>
#include <string>

int GetValue(std::string message_to_print, int min, int max, bool clear_after_mistake);
