#include "header.h"

std::string g_input_file_name = "test.txt"; // should work on every file and hopefully every file size
std::string g_output_file_name = "output.txt";
std::string g_transmitted_file_name = "transmitted.txt";
// for CRC
const char polynomial = 0xB0;
const int polynomial_length = 4;

int main()
{
	bool noise_setting = true;
	int menu_select;
	for (;;)
	{
		std::cout
			<< "|---------------------------------|\n"
			<< "|           Main Menu             |\n"
			<< "|---------------------------------|\n"
			<< "| 1: Parity bit                   |\n"
			<< "| 2: Checksum modulo 128          |\n"
			<< "| 3: CRC Checksum                 |\n"
			<< "| 4: Noise mode:                  |\n";
		if (noise_setting == true)
			std::cout << "|    with repeats                 |\n";
		else
			std::cout << "|    without repeats              |\n";
		std::cout
			<< "|                                 |\n"
			<< "| 0: Exit                         |\n"
			<< "|---------------------------------|\n";

		menu_select = GetValue("   Select menu option: ", 0, 4, false);

		switch (menu_select)
		{
		case 1:
			AddCheckFunction(ADD, PARITYBIT);
			AddCheckFunction(TRANSMIT, NOISE, true, 100);
			AddCheckFunction(CHECK, PARITYBIT);
			break;
		case 2:
			AddCheckFunction(ADD, CHECKSUM);
			AddCheckFunction(TRANSMIT, NOISE, true, 100);
			AddCheckFunction(CHECK, CHECKSUM);
			break;
		case 3:
			AddCheckFunction(ADD, CRC);
			AddCheckFunction(TRANSMIT, NOISE, true, 100);
			AddCheckFunction(CHECK, CRC);
			break;
		case 4:
			noise_setting = !noise_setting;
			break;
		case 0:
			return true;
			break;
		default:
			return false;
		}
	}
	return 0;
}