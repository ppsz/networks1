#include "header.h"

void ChecksumCRC(unsigned char* buffer, int &packet_length, char &check_byte)
{
	auto width = (8 * sizeof(polynomial));
	auto top_bit = (1 << (width - 1));

	for (int i = 0; i < packet_length; ++i)
	{
		unsigned char message = buffer[i];
		check_byte ^= (message << (width - 8));

		for (int j = 8; j > 0; --j)
		{
			if (check_byte & top_bit)
			{
				check_byte = (check_byte << 1) ^ polynomial;
			}
			else
			{
				check_byte = (check_byte << 1);
			}
		}
	}
}
