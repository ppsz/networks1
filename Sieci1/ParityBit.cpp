#include "header.h"

unsigned long long ParityBit(unsigned char* buffer, int packet_length)
{
	unsigned long long check_bytes;
	__asm
	{
		MOV EAX, DWORD PTR[buffer]
		MOV ECX, packet_length
			FORLOOP :
		ADD BYTE PTR[EAX], 0
			JP NOPARITY
			ADD check_bytes, 1
			NOPARITY :
			ADD EAX, 1
			DEC ECX
			JNZ FORLOOP
	};

	return check_bytes;
}