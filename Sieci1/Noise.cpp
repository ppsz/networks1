#include "header.h"

inline bool DrawCheck(unsigned long long random_bit_number, std::vector<unsigned long long> &drawn_numbers, int iterator)
{
	for (int i = 0; i <= iterator; ++i)
		if (drawn_numbers[i] == random_bit_number)
			return true;
	return false;
}

std::vector<unsigned long long> DrawNumbers(bool repetition, short percent, unsigned long long &file_length)
{
	std::random_device rd;
	std::mt19937 mt(1337);
	std::vector<unsigned long long> drawn_numbers;
	unsigned long long byte_number, random_bit_number, noise_percentage;
	unsigned char bit_in_byte_number;

	std::uniform_int_distribution<unsigned long long> dist(0, file_length * 8);
	noise_percentage = (file_length * 8) / percent;

	//Draw number to noise
	if (repetition == false)
	{
		for (int i = 0; i < noise_percentage; ++i)
		{
			random_bit_number = dist(mt);
			if (DrawCheck(random_bit_number, drawn_numbers, i) == true)
				random_bit_number = dist(mt);
			drawn_numbers.push_back(random_bit_number);
		}
	}
	else
	{
		for (int i = 0; i < noise_percentage; ++i)
		{
			random_bit_number = dist(mt);
			drawn_numbers.push_back(random_bit_number);
		}
	}
	std::sort(drawn_numbers.begin(), drawn_numbers.end());
	return drawn_numbers;
}

void Noise(unsigned char* buffer, int &packet_length, std::vector<unsigned long long> &drawn_numbers, unsigned int &iterator, int &current_number_index)
{
	const unsigned char bit_mask[8] = { 128, 64, 32, 16, 8, 4, 2, 1 };
	int byte_number, random_bit_number;
	unsigned char bit_in_byte_number;

	while (iterator == drawn_numbers[current_number_index] / (packet_length*8))
	{
		byte_number = (drawn_numbers[current_number_index] - packet_length * iterator) / 8;
		bit_in_byte_number = (drawn_numbers[current_number_index] - packet_length * iterator) % 8;
		current_number_index++;
		buffer[byte_number] ^= bit_mask[bit_in_byte_number];
		if (current_number_index == drawn_numbers.size())
			break;
	}
}