#include "header.h"

bool AddCheckFunction(AddCheck add_or_check, Function function, bool repetition, short percentage)
{
	std::ifstream input_file;
	std::ofstream output_file;
	std::string input_file_name, output_file_name;
	std::vector<unsigned long long> drawn_numbers;
	int number_of_packets, packet_length_remainder;
	int packet_length = (std::numeric_limits<int>::max() / 2);
	int file_position = packet_length;
	static int current_number_index = 0;
	unsigned long long file_length, check_bytes = 0, packet_check_bytes = 0;
	unsigned char* buffer;
	char check_byte = 0, check_byte_input = 0;

	switch (add_or_check)
	{
	case ADD:
		input_file_name = g_input_file_name;
		output_file_name = g_output_file_name;
		break;
	case CHECK:
		input_file_name = g_transmitted_file_name;
		//input_file_name = g_output_file_name;
		break;
	case TRANSMIT:
		input_file_name = g_output_file_name;
		output_file_name = g_transmitted_file_name;
		break;
	}

	// Opening input files
	input_file.open(input_file_name, std::ios::in | std::ios::binary);
	if (!input_file.is_open()) std::cout << "Error while opening file: " << input_file_name;
	if (add_or_check != CHECK)
	{
		output_file.open(output_file_name, std::ios::out | std::ios::binary | std::ios::trunc);
		if (!output_file.is_open()) std::cout << "Error while opening file: " << output_file_name;
		output_file.write("", 0);
		output_file.close();
	}
	// Taking length of the file
	input_file.seekg(0, input_file.end);
	file_length = input_file.tellg();
	if (add_or_check == CHECK)
	{
		file_length--;
		input_file.seekg(file_length);
		input_file.read(&check_byte_input, 1);
	}
	input_file.seekg(0, input_file.beg);
	input_file.close();

	if (function == NOISE)
		drawn_numbers = DrawNumbers(repetition, percentage, file_length);
		

	// Creating buffer
	number_of_packets = file_length / packet_length;
	packet_length_remainder = file_length % packet_length;
	if (number_of_packets > 0)
		buffer = new unsigned char[packet_length + 1];
	else
	{
		packet_length = packet_length_remainder;
		buffer = new unsigned char[packet_length + 1];
	}

	//---
	for (unsigned int i = 0; i <= number_of_packets; ++i)
	{
		if (i == number_of_packets && packet_length_remainder > 0 && packet_length_remainder != packet_length)
		{
			packet_length = packet_length_remainder;
		}
		// Reading from file to buffer
		input_file.open(input_file_name, std::ios::in | std::ios::binary);
		if (!input_file.is_open()) std::cout << "Error while opening file: " << input_file_name;
		// Changing reading position
		input_file.seekg((file_position*i), std::ios::cur);
		input_file.read(reinterpret_cast<char*>(buffer), packet_length);
		if (!input_file) std::cout << "Error, only: " << input_file.gcount() << " chars could be read\n";
		input_file.close();

		// Executing check functions
		switch (function)
		{
		case PARITYBIT:
			check_bytes = ParityBit(buffer, packet_length);
			break;
		case CHECKSUM:
			check_bytes = Checksum(buffer, packet_length);
			break;
		case CRC:
			ChecksumCRC(buffer, packet_length, check_byte);
			break;
		case NOISE:
			Noise(buffer, packet_length, drawn_numbers, i, current_number_index);
			break;
		}
		// Copying file
		if (add_or_check != CHECK)
		{
			output_file.open(output_file_name, std::ios::in | std::ios::binary | std::ios::ate);
			if (!output_file.is_open()) std::cout << "Error while opening file: " << output_file_name;
			output_file.write(reinterpret_cast<char*>(buffer), packet_length);
			output_file.close();
		}
	}
	switch (function)
	{
	case PARITYBIT:
		check_byte = (check_bytes % 2 == 0 ? 0 : 1);
		break;
	case CHECKSUM:
		check_byte = check_bytes % 128;
		break;
	case CRC:
		check_byte >>= (polynomial_length - 1);
		break;
	}
	switch (add_or_check)
	{
	case ADD:
		// Writing check byte at the end of the file
		output_file.open(output_file_name, std::ios::in | std::ios::binary | std::ios::ate);
		if (!output_file.is_open()) std::cout << "Error while opening file: " << output_file_name;
		output_file.write(&check_byte, 1);
		output_file.close();
		break;
	case CHECK:
		std::cout << (check_byte_input == check_byte ? "Valid\n" : "Invalid\n");
	}

	// Cleaning
	current_number_index = 0;
	delete[] buffer;
	buffer = NULL;
	return true;
}